package com.example.abulat.jsontask;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    private RecyclerView recyclerView;
    RuntimeExceptionDao<Job, Integer> cont;
    DatabaseHelper helper;
    OrmManager ormManager;
    private UsersAdapter mAdapter;
    private List<Job> friedlistList = new ArrayList<>();
    ProgressDialog progress;
    NetworkManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();
        getUsers();
        connectionInternet();
    }

    public void getUsers() {
        manager = new NetworkManager(this, new ApiCallBack() {
            @Override
            public void onSuccess(final ArrayList<Job> jobs) {
                progress.dismiss();
                ormManager.insertJobs(jobs);
                friedlistList = jobs;
                addRecyclerview();
                closeDB();
                Toast.makeText(getApplicationContext(), R.string.connected, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Exception e) {
                retry();
                progress.dismiss();
               friedlistList= ormManager.getJobsFromDb();
                addRecyclerview();
            }
        });
        manager.requestAllJobs();
    }

    public void init() {
        ormManager = new OrmManager(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        myRecyclerView.setLayoutManager(mLayoutManager);
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        progress = ProgressDialog.show(this, getString(R.string.wait),
                "", true);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void addRecyclerview() {
        mAdapter = new UsersAdapter(MainActivity.this, friedlistList);
        myRecyclerView.setAdapter(mAdapter);
    }

    public void connectionInternet() {
          progress.dismiss();
        if (!isNetworkAvailable()) {
            friedlistList = ormManager.getJobsFromDb();
            addRecyclerview();
            closeDB();
            Toast.makeText(getApplicationContext(), R.string.disconnected, Toast.LENGTH_SHORT).show();
            retry();
        }
    }

    public void closeDB() {
        ormManager.closeDb();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        closeDB();
    }

    public void retry() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.sorry_disconnection_internet))
                .setMessage(getString(R.string.dowantyouretrynow))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                      //  manager.retry();

                    }
                })
                .setNegativeButton(getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

}










