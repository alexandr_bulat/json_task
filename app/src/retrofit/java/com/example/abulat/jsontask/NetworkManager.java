package com.example.abulat.jsontask;

import android.content.Context;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by abulat on 7/21/16.
 */
public class NetworkManager extends Api {
    public static final String Friend = "jobs";
    Retrofit retrofit;
    public NetworkManager(Context context, ApiCallBack callback) {
        super(context, callback);
    }
    public interface GetJobsInterface {
        @GET(Friend)
        Call<List<Job>> getList();
    }
    @Override
    public void requestAllJobs() {
                 retrofit = new Retrofit.Builder()
                .baseUrl(context.getResources().getString(R.string.url_retrofit))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
                 retry();
    }
    public void  retry(){
        GetJobsInterface myApi = retrofit.create(GetJobsInterface.class);
        Call<List<Job>> friend = myApi.getList();
        friend.enqueue(new Callback<List<Job>>() {
            @Override
            public void onResponse(Call<List<Job>> call, Response<List<Job>> response) {
                callback.onSuccess((ArrayList<Job>) response.body());
            }
            @Override
            public void onFailure(Call<List<Job>> call, Throwable t) {

            }
        });
    }
}