package com.example.abulat.jsontask;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by abulat on 7/22/16.
 */
public class DatabaseConfigUtill extends OrmLiteConfigUtil {
    private static final Class<?>[] classes = new Class[]{Job.class};

    public static void main(String[] args) throws SQLException, IOException {
        writeConfigFile("ormlite", classes);
    }

}
