package com.example.abulat.jsontask;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


/**
 * Created by abulat on 7/25/16.
 */
public class MapPane extends FragmentActivity implements OnMapReadyCallback {
    Job ci;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);
        init();
    }

    @Override
    public void onMapReady(GoogleMap map) {

        LatLng sydney = new LatLng(Double.parseDouble(ci.getJobLatitude()), Double.parseDouble(ci.getJobLongitude()));
        map.addMarker(new MarkerOptions().position(sydney).title(ci.getCustomerName()).snippet(sydney.toString()));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12));
        mMap.setOnMyLocationChangeListener(myLocationChangeListener);
    }

    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter() {
            myContentsView = getLayoutInflater().inflate(R.layout.custom_info_contents, null);
        }

        @Override
        public View getInfoContents(Marker marker) {
            TextView tvTitle = ((TextView) myContentsView.findViewById(R.id.title));
            tvTitle.setText(marker.getTitle());
            TextView tvSnippet = ((TextView) myContentsView.findViewById(R.id.snippet));
            tvSnippet.setText(marker.getSnippet());
            TextView textStreet = ((TextView) myContentsView.findViewById(R.id.street));
            textStreet.setText(ci.getJobStreet());

            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {

            return null;
        }
    }

    public void init() {
        Intent i = getIntent();
        ci = i.getExtras().getParcelable("Friend");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mMap = mapFragment.getMap();
        mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
    }

    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.addMarker(new MarkerOptions().position(loc));
            if (mMap != null) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
            }
        }
    };
}


