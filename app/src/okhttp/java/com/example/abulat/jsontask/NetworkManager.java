package com.example.abulat.jsontask;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by abulat on 7/21/16.
 */
public class NetworkManager extends Api {
    Context context;

    public NetworkManager(Context context, ApiCallBack callback) {
        super(context, callback);
        this.context = context;
    }

    private class RequestTask extends AsyncTask<Void, Void, String> {
        private OkHttpClient client;
        private Request request;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            client = new OkHttpClient();
            request = new Request.Builder()
                    .url(context.getResources().getString(R.string.url))
                    .build();
        }

        @Override
        protected String doInBackground(Void... voids) {
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                callback.onFailure(e);
            }
            try {
                if (response != null) {
                    return response.body().string();
                }
            } catch (IOException e) {
                callback.onFailure(e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.isEmpty()) {
                NetworkManager.this.callback.onSuccess(new ArrayList<Job>());
            }else {
                NetworkManager.this.callback.onSuccess(JsonParse.jsonParse(s.toString()));
            }
        }
    }

    @Override
    public void requestAllJobs() {
        RequestTask requestTask = new RequestTask();
        requestTask.execute();
    }

}
