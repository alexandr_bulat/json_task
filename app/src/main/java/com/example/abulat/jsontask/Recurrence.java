package com.example.abulat.jsontask;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abulat on 7/25/16.
 */
enum Recurrence {
    @SerializedName("0")
    @Expose
    Once("Once"),

    @SerializedName("7")
    @Expose
    Weekly("Weekly"),

    @SerializedName("14")
    @Expose
    EveryTwoWeeks("Every two weeks"),

    @SerializedName("28")
    @Expose
    Monthly("Monthly");

    private String name;

    Recurrence(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
