package com.example.abulat.jsontask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by abulat on 7/22/16.
 */
public class ParametersActivity extends Activity {
    TextView textView;
    Button buttonmap;
    Job ci;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paramets_activity);
        getUsers();
    }

    public void getUsers() {
        Intent i = getIntent();
        ci = i.getExtras().getParcelable("Friend");
        textView = (TextView) findViewById(R.id.getText);
        buttonmap = (Button) findViewById(R.id.buttonmap);
        buttonmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ParametersActivity.this, MapPane.class);
                intent.putExtra("Friend", ci);
                startActivity(intent);
            }
        });
        textView.setText(ci.getJobStreet());
    }
}
