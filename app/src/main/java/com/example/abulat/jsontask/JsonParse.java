package com.example.abulat.jsontask;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by abulat on 7/27/16.
 */
public class JsonParse {
    public static ArrayList<Job> jsonParse(String responseResult) {
        ArrayList<Job> jobs = new ArrayList<>();
        Gson gson = new Gson();
        try {
            JSONArray jsonArray = new JSONArray(responseResult);
            for (int i = 0; i < jsonArray.length(); ++i) {
                JSONObject jsonJob = jsonArray.getJSONObject(i);
                Job job = gson.fromJson(jsonJob.toString(), Job.class);
                jobs.add(job);
            }
        } catch (JSONException e) {

        }
        return jobs;
    }
}
