package com.example.abulat.jsontask;

import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by abulat on 7/26/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class Volley extends ActivityInstrumentationTestCase2<MainActivity> {
    NetworkManager manager;
    OrmManager ormManager;
    DatabaseHelper helper;
    Context mActivity;
    @Mock
    MainActivity mainActivity;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    public Volley() {
        super("com.example.abulat.jsontask", MainActivity.class);
    }

    @Test
    public void getVolley() {
        mainActivity.init();
        manager = new NetworkManager(mainActivity, new ApiCallBack() {
            @Override
            public void onSuccess(ArrayList<Job> jobs) {
                ormManager.insertJobs(jobs);
                mainActivity.addRecyclerview();
                assertNotNull(ormManager.getJobsFromDb());
                ormManager.closeDb();
            }

            @Override
            public void onFailure(Exception e) {

            }
        });


    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mainActivity = this.getActivity();
        this.helper = OpenHelperManager.getHelper(mainActivity, DatabaseHelper.class);
        ormManager = new OrmManager(mainActivity);

    }
}
