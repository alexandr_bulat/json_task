package com.example.abulat.jsontask;

/**
 * Created by abulat on 7/22/16.
 */
public interface Mydata {

    public String getStatus();

    /**
     * @param status The status
     */
    public void setStatus(String status);

    /**
     * @return The customerName
     */
    public String getCustomerName();

    /**
     * @param customerName The customer_name
     */
    public void setCustomerName(String customerName);

    /**
     * @return The distance
     */
    public String getDistance();

    /**
     * @param distance The distance
     */
    public void setDistance(String distance);

    /**
     * @return The jobDate
     */
    public String getJobDate();

    /**
     * @param jobDate The job_date
     */
    public void setJobDate(String jobDate);

    /**
     * @return The extras
     */
    public String getExtras();

    /**
     * @param extras The extras
     */
    public void setExtras(String extras);

    /**
     * @return The orderDuration
     */
    public double getOrderDuration();

    /**
     * @param orderDuration The order_duration
     */
    public void setOrderDuration(double orderDuration);

    /**
     * @return The orderId
     */
    public String getOrderId();

    /**
     * @param orderId The order_id
     */
    public void setOrderId(String orderId);

    /**
     * @return The orderTime
     */
    public String getOrderTime();

    /**
     * @param orderTime The order_time
     */
    public void setOrderTime(String orderTime);

    /**
     * @return The paymentMethod
     */
    public String getPaymentMethod();

    /**
     * @param paymentMethod The payment_method
     */
    public void setPaymentMethod(String paymentMethod);

    /**
     * @return The price
     */
    public String getPrice();

    /**
     * @param price The price
     */
    public void setPrice(String price);

    /**
     * @return The recurrency
     */
    public String getRecurrency();

    /**
     * @param recurrency The recurrency
     */
    public void setRecurrency(Recurrence recurrency);

    /**
     * @return The jobCity
     */
    public String getJobCity();

    /**
     * @param jobCity The job_city
     */
    public void setJobCity(String jobCity);

    /**
     * @return The jobLatitude
     */
    public String getJobLatitude();

    /**
     * @param jobLatitude The job_latitude
     */
    public void setJobLatitude(String jobLatitude);

    /**
     * @return The jobLongitude
     */
    public String getJobLongitude();

    /**
     * @param jobLongitude The job_longitude
     */
    public void setJobLongitude(String jobLongitude);

    /**
     * @return The jobPostalcode
     */
    public Integer getJobPostalcode();

    /**
     * @param jobPostalcode The job_postalcode
     */
    public void setJobPostalcode(Integer jobPostalcode);

    /**
     * @return The jobStreet
     */
    public String getJobStreet();

    /**
     * @param jobStreet The job_street
     */
    public void setJobStreet(String jobStreet);

}
