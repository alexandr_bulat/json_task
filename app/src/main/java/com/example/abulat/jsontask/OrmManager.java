package com.example.abulat.jsontask;

import android.content.Context;
import android.database.SQLException;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abulat on 7/25/16.
 */
public class OrmManager {
    private Context context;
    private DatabaseHelper helper;
    private RuntimeExceptionDao<Job, Integer> jobDao;

    public OrmManager(Context context) {
        this.context = context;
        try {
            this.helper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
            this.jobDao = helper.getDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Job> getJobsFromDb() throws SQLException {
        List<Job> list = new ArrayList<>();
        list = jobDao.queryForAll();

        return list;
    }

    public void insertJobs(List<Job> jobs) throws SQLException {
        List<Job> dbList = getJobsFromDb();
        if (dbList.size() != 0) {
            for (int i = 0; i < jobs.size(); i++) {
                if (!dbList.get(i).getCustomerName().equals(jobs.get(i).getCustomerName())) {
                    jobDao.create(jobs.get(i));
                }
            }
        } else {
            insertAllJobs(jobs);
        }
    }

    private void insertAllJobs(List<Job> jobs) throws SQLException {
        for (int i = 0; i < jobs.size(); i++) {
            jobDao.create(jobs.get(i));
        }
    }

    public void closeDb() {
        helper.close();
        OpenHelperManager.releaseHelper();
    }
}

