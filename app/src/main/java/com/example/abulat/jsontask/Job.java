package com.example.abulat.jsontask;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by abulat on 7/21/16.
 */
public class Job implements Parcelable {
    public static final String FIELD_NAME_ID = "id";
    @DatabaseField(columnName = FIELD_NAME_ID, generatedId = true)
    int id;
    @DatabaseField(columnName = "customer_name")
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @DatabaseField(columnName = "distance")
    @SerializedName("distance")
    @Expose
    private String distance;
    @DatabaseField(columnName = "job_date")
    @SerializedName("job_date")
    @Expose
    private String jobDate;
    @DatabaseField(columnName = "extras")
    @SerializedName("extras")
    @Expose
    private String extras;
    @DatabaseField(columnName = "order_duration")
    @SerializedName("order_duration")
    @Expose
    private double orderDuration;
    @DatabaseField(columnName = "order_id")
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @DatabaseField(columnName = "order_time")
    @SerializedName("order_time")
    @Expose
    private String orderTime;
    @DatabaseField(columnName = "payment_method")
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @DatabaseField(columnName = "price")
    @SerializedName("price")
    @Expose
    private String price;
    @DatabaseField(columnName = "recurrency")
    @SerializedName("recurrency")
    @Expose
    private Recurrence recurrency;
    @DatabaseField(columnName = "job_city")
    @SerializedName("job_city")
    @Expose
    private String jobCity;
    @DatabaseField(columnName = "job_latitude")
    @SerializedName("job_latitude")
    @Expose
    private String jobLatitude;
    @DatabaseField(columnName = "job_longitude")
    @SerializedName("job_longitude")
    @Expose
    private String jobLongitude;
    @DatabaseField(columnName = "job_postalcode")
    @SerializedName("job_postalcode")
    @Expose
    private Integer jobPostalcode;
    @DatabaseField(columnName = "job_street")
    @SerializedName("job_street")
    @Expose
    private String jobStreet;
    @DatabaseField(columnName = "status")
    @SerializedName("status")
    @Expose
    private String status;

    public Job() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getJobDate() {
        return jobDate;
    }

    public void setJobDate(String jobDate) {
        this.jobDate = jobDate;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public double getOrderDuration() {
        return orderDuration;
    }

    public void setOrderDuration(double orderDuration) {
        this.orderDuration = orderDuration;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRecurrency() {
        return this.recurrency.getName();
    }

    public void setRecurrency(Recurrence recurrency) {
        this.recurrency = recurrency;
    }

    public String getJobCity() {
        return jobCity;
    }

    public void setJobCity(String jobCity) {
        this.jobCity = jobCity;
    }

    public String getJobLatitude() {
        return jobLatitude;
    }

    public void setJobLatitude(String jobLatitude) {
        this.jobLatitude = jobLatitude;
    }

    public String getJobLongitude() {
        return jobLongitude;
    }

    public void setJobLongitude(String jobLongitude) {
        this.jobLongitude = jobLongitude;
    }

    public Integer getJobPostalcode() {
        return jobPostalcode;
    }

    public void setJobPostalcode(Integer jobPostalcode) {
        this.jobPostalcode = jobPostalcode;
    }

    public String getJobStreet() {
        return jobStreet;
    }

    public void setJobStreet(String jobStreet) {
        this.jobStreet = jobStreet;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.status);
        dest.writeString(this.customerName);
        dest.writeString(this.distance);
        dest.writeString(this.jobDate);
        dest.writeString(this.extras);
        dest.writeValue(this.orderDuration);
        dest.writeString(this.orderId);
        dest.writeString(this.orderTime);
        dest.writeString(this.paymentMethod);
        dest.writeString(this.price);
        dest.writeValue(this.recurrency);
        dest.writeString(this.jobCity);
        dest.writeString(this.jobLatitude);
        dest.writeString(this.jobLongitude);
        dest.writeValue(this.jobPostalcode);
        dest.writeString(this.jobStreet);
    }

    protected Job(Parcel in) {
        this.status = in.readString();
        this.customerName = in.readString();
        this.distance = in.readString();
        this.jobDate = in.readString();
        this.extras = in.readString();
        this.orderDuration = (double) in.readValue(double.class.getClassLoader());
        this.orderId = in.readString();
        this.orderTime = in.readString();
        this.paymentMethod = in.readString();
        this.price = in.readString();
        this.recurrency = (Recurrence) in.readValue(Recurrence.class.getClassLoader());
        this.jobCity = in.readString();
        this.jobLatitude = in.readString();
        this.jobLongitude = in.readString();
        this.jobPostalcode = (Integer) in.readValue(Integer.class.getClassLoader());
        this.jobStreet = in.readString();
    }

    public static final Parcelable.Creator<Job> CREATOR = new Parcelable.Creator<Job>() {
        @Override
        public Job createFromParcel(Parcel source) {
            return new Job(source);
        }

        @Override
        public Job[] newArray(int size) {
            return new Job[size];
        }
    };
}
