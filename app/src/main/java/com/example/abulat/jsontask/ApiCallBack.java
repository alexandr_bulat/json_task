package com.example.abulat.jsontask;

import java.util.ArrayList;

/**
 * Created by abulat on 7/21/16.
 */
public interface ApiCallBack {
    void onSuccess(ArrayList<Job> jobs);
    void onFailure(Exception e);
}
