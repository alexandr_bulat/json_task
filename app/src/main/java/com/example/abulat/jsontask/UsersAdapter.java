package com.example.abulat.jsontask;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by abulat on 7/21/16.
 */
public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.MyViewHolder> {
    private List<Job> job;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView order_durition;
        private TextView status;
        private TextView customerName;
        private TextView distance;
        private TextView jobDate;
        private TextView orderId;
        private TextView orderTime;
        private TextView paymentMethod;
        private TextView price;
        private TextView recurrency;
        private TextView jobCity;
        private TextView jobLatitude;
        private TextView jobLongitude;
        private TextView jobPostalcode;
        private TextView jobStreet;

        public MyViewHolder(View view) {
            super(view);
            init(view);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ParametersActivity.class);
                    Job friendList = job.get(getAdapterPosition());
                    intent.putExtra("Friend", (Parcelable) friendList);
                    context.startActivity(intent);
                }
            });
        }

        public void init(View view) {
            title = (TextView) view.findViewById(R.id.info_text);
            order_durition = (TextView) view.findViewById(R.id.order_duration);
            status = (TextView) view.findViewById(R.id.status);
            customerName = (TextView) view.findViewById(R.id.customer_name);
            distance = (TextView) view.findViewById(R.id.distance);
            jobDate = (TextView) view.findViewById(R.id.job_date);
            orderId = (TextView) view.findViewById(R.id.order_id);
            orderTime = (TextView) view.findViewById(R.id.order_time);
            paymentMethod = (TextView) view.findViewById(R.id.payment_method);
            price = (TextView) view.findViewById(R.id.price);
            recurrency = (TextView) view.findViewById(R.id.recurrency);
            jobCity = (TextView) view.findViewById(R.id.job_city);
            jobLatitude = (TextView) view.findViewById(R.id.job_latitude);
            jobLongitude = (TextView) view.findViewById(R.id.job_longitude);
            jobPostalcode = (TextView) view.findViewById(R.id.job_postalcode);
            jobStreet = (TextView) view.findViewById(R.id.job_street);
        }
    }

    public UsersAdapter(Context context, List<Job> job) {
        this.job = job;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_of_data, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Job jobs = job.get(position);
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        java.util.Date date = null;
        try
        {
            date = form.parse(jobs.getJobDate());
        }
        catch (ParseException e)
        {

            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("MMMMM dd, yyyy");
        String newDateStr = postFormater.format(date);
        holder.title.setText(jobs.getExtras());
        holder.order_durition.setText(String.valueOf(jobs.getOrderDuration()));
        holder.status.setText(jobs.getStatus());
        holder.customerName.setText(jobs.getCustomerName());
        holder.distance.setText(jobs.getDistance());
        holder.jobDate.setText(newDateStr);
        holder.orderId.setText(jobs.getOrderId());
        holder.orderTime.setText(jobs.getOrderTime());
        holder.paymentMethod.setText(jobs.getPaymentMethod());
        holder.price.setText(jobs.getPrice());
        holder.recurrency.setText(jobs.getRecurrency());
        holder.jobCity.setText(jobs.getJobCity());
        holder.jobLatitude.setText(jobs.getJobLatitude());
        holder.jobLongitude.setText(jobs.getJobLongitude());
        holder.jobPostalcode.setText(String.valueOf(jobs.getJobPostalcode()));
        holder.jobStreet.setText(jobs.getJobStreet());
    }

    @Override
    public int getItemCount() {
        return job.size();
    }
}


