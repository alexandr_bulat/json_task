package com.example.abulat.jsontask;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by abulat on 7/21/16.
 */
public class NetworkManager extends Api {
    Context context;

    public NetworkManager(Context context, ApiCallBack callback) {
        super(context, callback);
        this.context = context;
    }

    @Override
    public void requestAllJobs() {
        JsonArrayRequest billionaireReq = new JsonArrayRequest(context.getResources().getString(R.string.url),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        callback.onSuccess(JsonParse.jsonParse(response.toString()));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkManager.this.callback.onFailure(error);
            }
        });
        AppController.getInstance(context).addToRequestQueue(billionaireReq);
    }


}
