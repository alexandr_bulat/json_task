package com.example.abulat.jsontask;

import android.content.Context;

/**
 * Created by abulat on 7/21/16.
 */
public abstract class Api {

    ApiCallBack callback;
    Context context;

    public Api(Context context, ApiCallBack callback) {
        this.callback = callback;
        this.context = context;
    }

    public abstract void requestAllJobs();
}
