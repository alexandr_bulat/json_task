package com.example.abulat.jsontask;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by abulat on 7/22/16.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "ormlive.db";
    private static final int DATABASE_VERSION = 1;
    private Dao<Job, Integer> mUserDao = null;
    private RuntimeExceptionDao<Job, Integer> cont;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Job.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Job.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Dao<Job, Integer> getUserDao() throws SQLException {
        if (mUserDao == null) {
            mUserDao = getDao(Job.class);
        }
        return mUserDao;
    }

    public RuntimeExceptionDao<Job, Integer> getDao() {
        if (cont == null) {
            cont = getRuntimeExceptionDao(Job.class);

        }
        return cont;
    }

    @Override
    public void close() {
        mUserDao = null;
        super.close();
    }
}
