package com.example.abulat.jsontask;

import android.app.Application;
import android.content.Context;

/**
 * Created by abulat on 7/27/16.
 */
public class MyApplication extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
        MyApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MyApplication.context;
    }
}
